/*
 * Copyright (C) 2020 Krifa75
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Tracker;
public class Files.Plugins.Medias : Files.Plugins.Base {
    private Files.AbstractSlot slot_view;

    private Gtk.Box box_medias;

    private Gtk.Scale seekbar;
    private Gtk.Label media_name;
    private Gtk.Label current_progress;

    private Gtk.Button play_pause;
    private Gtk.Image player_start;
    private Gtk.Image player_pause;

    private Gst.Bus bus;
    private Gst.Pipeline pipeline;
    private Gst.Element src;

    private Gee.HashMap <GLib.File, GLib.FileMonitor> monitors;
    private GLib.List <GLib.File> playlist;
    private GLib.File current_playing;

    private Gst.ClockTime duration;
    private int64 position;
    private ulong value_changed_id;
    private uint source_player;

    private Gtk.Widget image_previewer;

    public Medias () {
        /* Initiliaze gettext support */
        Intl.setlocale (LocaleCategory.ALL, "");
        box_medias = null;
    }

    private static inline bool GST_CLOCK_TIME_IS_VALID (Gst.ClockTime time) {
        return ((time) != Gst.CLOCK_TIME_NONE);
    }

    private void construct_media_player () {
        unowned string[]? args = null;
        Gst.init (ref args);

        monitors = new Gee.HashMap <GLib.File, GLib.FileMonitor> ();
        current_playing = null;
        playlist = new GLib.List <GLib.File> ();
        duration = Gst.CLOCK_TIME_NONE;

        pipeline = new Gst.Pipeline ("pipeline");

        src = Gst.ElementFactory.make ("filesrc", "filesrc");
        Gst.Element decode = Gst.ElementFactory.make ("decodebin", "decode");
        Gst.Element converter = Gst.ElementFactory.make ("audioconvert", "converter");
        Gst.Element sink = Gst.ElementFactory.make("autoaudiosink", "sink");

        decode.pad_added.connect ( (element, pad) => {
            Gst.Pad pad_sink = decode.get_static_pad ("sink");
            element.link (converter);
            pad.link (pad_sink);
        });

        pipeline.add_many (src, decode, converter, sink);
        bus = pipeline.get_bus ();

        if (src.link(decode) != true) {
            critical ("Elements source and decode could not be linked");
            return;
        }

        if (converter.link (sink) != true) {
            critical ("Elements converter and sink could not be linked");
            return;
        }

        image_previewer = null;
        box_medias = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 10);

        Gtk.Button backward  = new Gtk.Button.from_icon_name ("media-skip-backward") {
            margin_start=10
        };
        backward.get_style_context ().add_class ("circular");
        backward.clicked.connect (on_backward_cb);
        box_medias.add (backward);

        player_start = new Gtk.Image.from_icon_name ("media-playback-start", Gtk.IconSize.BUTTON);
        player_pause = new Gtk.Image.from_icon_name ("media-playback-pause", Gtk.IconSize.BUTTON);

        play_pause  = new Gtk.Button () { image=player_start }; //.from_icon_name ("media-playback-start");
        play_pause.get_style_context ().add_class ("circular");
        play_pause.clicked.connect (on_play_pause_cb);
        box_medias.add (play_pause);

        Gtk.Button stop  = new Gtk.Button.from_icon_name ("media-playback-stop");
        stop.get_style_context ().add_class ("circular");
        stop.clicked.connect (on_stop_cb);
        box_medias.add (stop);

        Gtk.Button forward  = new Gtk.Button.from_icon_name ("media-skip-forward");
        forward.get_style_context ().add_class ("circular");
        forward.clicked.connect (on_forward_cb);
        box_medias.add (forward);

        media_name = new Gtk.Label (null) {
            ellipsize=Pango.EllipsizeMode.END,
            max_width_chars=20
        };

        seekbar = new Gtk.Scale.with_range (Gtk.Orientation.HORIZONTAL, 0, 1, 0.1) {
            value_pos=Gtk.PositionType.RIGHT,
            can_focus=false,
            draw_value=false
        };

        current_progress = new Gtk.Label (Granite.DateTime.seconds_to_time (0));

        value_changed_id = seekbar.value_changed.connect (on_value_changed_cb);

        box_medias.pack_start (media_name, false, false);
        box_medias.pack_start (seekbar, true, true, 5);
        box_medias.pack_start (current_progress, false, false, 10);

        box_medias.get_style_context ().add_class (Granite.STYLE_CLASS_SEEKBAR);

        source_player = GLib.Timeout.add (1000, seekbar_change_value);
    }

    private bool found_message_from_bus () {
        Gst.Message msg = bus.pop_filtered (Gst.MessageType.ERROR | Gst.MessageType.EOS);
        bool msg_found = false;

        if (msg == null)
            return msg_found;

        switch (msg.type) {
            case Gst.MessageType.ERROR:
                unowned var media_selected = playlist.find_custom (current_playing, find_media_by_path);
                GLib.Error err;
                string debug_info;
                msg.parse_error (out err, out debug_info);
                warning ("Error received from element %s for file %s: %s\n", msg.src.name, current_playing.get_uri (), err.message);
                warning ("Debugging information: %s\n", (debug_info != null)? debug_info : "none");
                if (media_selected != null) {
                    monitors.unset (media_selected.data);
                    remove_media_from_playlist (media_selected);
                }
                msg_found = true;
                break;
            case Gst.MessageType.EOS:
                on_forward_cb ();
                msg_found = true;
                break;
            default:
                break;
        }
        return msg_found;
    }

    private bool seekbar_change_value () {
        Gst.State state;
        pipeline.get_state (out state, null, Gst.CLOCK_TIME_NONE);

        if (found_message_from_bus () || state < Gst.State.PAUSED)
            return GLib.Source.CONTINUE;

        if (!GST_CLOCK_TIME_IS_VALID (duration)) {
            if (!pipeline.query_duration (Gst.Format.TIME, out duration))
                critical ("Cannot get the duration for file : %s", current_playing.get_uri ());
            else {
                duration = duration / Gst.SECOND;
                seekbar.set_range (0, duration);
            }
        }

        if (pipeline.query_position (Gst.Format.TIME, out position)) {
            GLib.SignalHandler.block (seekbar, value_changed_id);
            position = position / Gst.SECOND;
            current_progress.label = Granite.DateTime.seconds_to_time ((int)position);
            seekbar.set_value (position);
            GLib.SignalHandler.unblock (seekbar, value_changed_id);
        }
        return GLib.Source.CONTINUE;
    }

    private void on_value_changed_cb () {
        if( media_name.label == "") {
            seekbar.set_value (0);
            return;
        }
        double current_value = seekbar.get_value ();
        pipeline.seek_simple (Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, (int64)(current_value * Gst.SECOND));
    }

    private bool file_is_media (Files.File file) {
        bool is_media = false;
        unowned string? content_type = file.get_ftype ();
        if (content_type != null) {
            is_media = GLib.ContentType.is_a (content_type, "audio/*") || GLib.ContentType.is_a (content_type, "video/*");
        }
        return is_media;
    }

    /**
     * Maybe by basename
    */
    private CompareFunc<GLib.File> find_media_by_path = (file_a, file_b) => {
        return (file_a.get_path () == file_b.get_path ()) ? 0 : 1;
    };

    private CompareDataFunc<GLib.File> insert_in_playlist = (file_a, file_b) => {
        int res = GLib.strcmp (file_a.get_basename ().down (), file_b.get_basename ().down ());
        return res;
    };

    private GLib.File? get_selected_file () {
        if (playlist == null)
            return null;
        unowned GLib.List <Files.File>? files = slot_view.get_selected_files ();
        unowned GLib.File selected_file;

        if (files == null || files.length () > 1)
            selected_file = playlist.data; // play the first;
        else if (file_is_media (files.data)){
            unowned var media_selected = playlist.find_custom (files.data.location, find_media_by_path);
            selected_file = media_selected.data;
        } else
            selected_file = playlist.data;
        return selected_file;
    }

    private void play_audio (GLib.File? audio = null, bool reset_time = true) {
        if (audio == null)
            current_playing = get_selected_file ();
        else {
            current_playing = audio;
        }
        pipeline.set_state (Gst.State.READY);
        src.set_property ("location", current_playing.get_path ());
        media_name.label = current_playing.get_basename ();

        play_pause.image = player_pause;
        duration = Gst.CLOCK_TIME_NONE;
        pipeline.set_state (Gst.State.PLAYING);

        if (reset_time)
            play_reset ();
    }

    private void play_reset () {
        if (seekbar.get_value () > 0) {
            GLib.SignalHandler.block (seekbar, value_changed_id);
            current_progress.label = Granite.DateTime.seconds_to_time (0);
            duration = Gst.CLOCK_TIME_NONE; // to update the duration;
            seekbar.set_value (0);
            GLib.SignalHandler.unblock (seekbar, value_changed_id);
        }
    }

    private void on_backward_cb () {
        unowned var current_media = playlist.find (current_playing);
        if (current_media != null) {
            if (current_media.prev != null)
                current_playing = current_media.prev.data;
            else {
                current_playing = playlist.last ().data;
            }
            play_audio (current_playing);
        }
    }

    private void on_play_pause_cb () {
        Gst.State state, new_state;
        pipeline.get_state (out state, null, Gst.CLOCK_TIME_NONE);

        switch (state) {
            case Gst.State.NULL:
                current_playing = get_selected_file ();
                if (current_playing == null)
                    return;
                src.set_property ("location", current_playing.get_path ());
                media_name.label = current_playing.get_basename ();
                new_state = Gst.State.PLAYING;
                break;
            case Gst.State.READY:
                if (current_playing.get_parent ().get_path () != slot_view.uri)
                    play_audio (null, false);
                new_state = Gst.State.PLAYING;
                break;
            case Gst.State.PLAYING:
                new_state = Gst.State.PAUSED;
                break;
            case Gst.State.PAUSED:
                new_state = Gst.State.PLAYING;
                break;
            default:
                critical ("New state not found");
                return;
        }

        play_pause.image = (new_state == Gst.State.PLAYING) ? player_pause : player_start;
        pipeline.set_state (new_state);
    }

    private void on_stop_cb () {
        if (current_playing == null)
            return;
        media_name.label = "";
        pipeline.set_state (Gst.State.READY);
        if (current_playing.get_parent ().get_uri () != slot_view.uri) {
            if (!slot_view.directory.contain_audios && !slot_view.directory.contain_videos) {
                quit_media_player ();
                return;
            } else {
                playlist = null;
                find_audios_with_tracker.begin (slot_view.uri);
            }
        }
        play_pause.image = player_start;
        play_reset ();
    }

    private void on_forward_cb () {
        unowned var current_media = playlist.find (current_playing);
        if (current_media == null) {
            warning ("Could not forward.");
            return;
        } else {
            if (current_media.next != null)
                current_playing = current_media.next.data;
            else {
                current_playing = playlist.first ().data;
            }

            play_audio (current_playing);
        }
    }

    private async bool fetch_next_results (Sparql.Cursor cursor) {
        try {
            if (!yield cursor.next_async ()) {
                // We fall in this case when the folder
                // is an external drive.
                if (playlist == null) {
                    slot_view.set_all_selected (true);
                    unowned var gofs = slot_view.get_selected_files ();
                    foreach (var gof in gofs) {
                        if (gof.is_audio () || gof.is_video ())
                            playlist.append (gof.location);
                    }
                    slot_view.set_all_selected (false);
                }
                return false;
            }

            GLib.File file = GLib.File.new_for_uri (cursor.get_string (0));
            playlist.append (file);

            create_monitor (file);
        } catch (Error e) {
            warning ("Could not fetch results : %s", e.message);
        }
        return yield fetch_next_results (cursor);
    }

    private async void find_audios_with_tracker (string uri) {
        Sparql.Connection connection;
        Sparql.Cursor cursor;

        string search_medias = "SELECT DISTINCT ?url WHERE {\n" +
                            "   ?audio a nfo:Audio ; nie:url ?url ; nfo:belongsToContainer ?container . ?container nie:url \"" + uri + "\"\n" +
                            "}";

        try {
            connection = Sparql.Connection.get ();
            if (connection == null)
                return;

            playlist = null;
            cursor = yield connection.query_async (search_medias);
            yield fetch_next_results (cursor);
        } catch (GLib.Error error) {
            critical ("Error connecting to Tracker: %s", error.message);
        }
    }

    private void create_monitor (GLib.File file) {
        GLib.FileMonitor new_monitor;
        try {
            new_monitor = file.monitor (GLib.FileMonitorFlags.WATCH_MOVES);
            new_monitor.changed.connect (on_file_changed);
            monitors.@set (file, new_monitor);
        } catch (Error e) {
            warning ("Cannot create monitor for the file %s : %s", file.get_uri (), e.message);
        }
    }

    private void on_file_changed (GLib.File file, GLib.File? other_file, GLib.FileMonitorEvent event) {
        if (current_playing != null && (current_playing.get_uri () != file.get_uri ()))
            return;

        unowned var media_selected = playlist.find_custom (file, find_media_by_path);
        switch (event) {
            case GLib.FileMonitorEvent.RENAMED:
                GLib.FileMonitor old_monitor;
                if (monitors.unset (media_selected.data, out old_monitor)) {
                    old_monitor.changed.disconnect (on_file_changed);
                    create_monitor (other_file);
                }

                media_selected.data = other_file;

                Gst.State state;
                pipeline.get_state (out state, null, Gst.CLOCK_TIME_NONE);
                // When renaming the player start, so we check if it is
                // playing to avoid this case
                if (state == Gst.State.PLAYING) {
                    current_playing = media_selected.data;
                    media_name.label = current_playing.get_basename ();
                }
                break;
            case GLib.FileMonitorEvent.MOVED_OUT:
                if (media_selected != null) {
                    monitors.unset (media_selected.data);
                    remove_media_from_playlist (media_selected);
                }
                break;
            default:
                break;
        }
    }

    private void remove_media_from_playlist (GLib.List media_selected) {
        if (playlist.length () > 1)
            on_forward_cb ();
        else if (playlist.length () == 1) /* It was the last one */
            quit_media_player ();
        playlist.delete_link (media_selected);
    }

    private void quit_media_player () {
        GLib.Source.remove (source_player);

        seekbar.value_changed.disconnect (on_value_changed_cb);

        pipeline.set_state (Gst.State.NULL);

        box_medias.destroy ();
        box_medias = null;

        monitors.clear ();
        monitors = null;

        image_previewer = null;
    }

    private void display_media_player () {
        GLib.return_if_fail (box_medias == null);

        construct_media_player ();
        slot_view.add_extra_action_widget (box_medias);
        slot_view.selection_changed.connect (media_selection_changed);
        box_medias.show_all ();

#if DISABLE_OPEN_AUDIOS
        var pantheon_toolbar = get_widget_by_name (window as Gtk.Widget, "toolbar") as Gtk.Toolbar;
        if (pantheon_toolbar != null) {
            var item = pantheon_toolbar.get_nth_item (1); /* Open */
            ((Gtk.ToolButton)item).clicked.connect (play_audio_on_open_click);
        }
#endif
    }

    private void on_file_added (Files.File? file) {
        if (file == null || !file_is_media (file))
            return;

        if (box_medias == null)
            display_media_player ();

        if (playlist == null)
            playlist.append (file.location);
        else
            playlist.insert_sorted_with_data (file.location,  insert_in_playlist);

        create_monitor (file.location);
    }

    public override void directory_loaded (Gtk.ApplicationWindow window, Files.AbstractSlot view, Files.File directory) {
        slot_view = view;

        // Recliking on an opened directory give two different uri.
        // The view give the parent and directory the right uri.
        // In our case if the parent contains some medias and not the directory the seekbar can appear
        if (slot_view.uri != directory.uri)
            return;

        view.directory.file_added.connect (on_file_added);

        if ( (view.directory.contain_audios || view.directory.contain_videos) && !view.directory.is_trash) {
            if (box_medias == null)
                display_media_player ();

            if (playlist == null || media_name.label == "") {
                find_audios_with_tracker.begin (slot_view.uri, (obj, res) => {
                    box_medias.show_all ();
                });
            }
        } else if (box_medias != null && media_name.label == "") {
            quit_media_player ();
        }
    }

    private void media_selection_changed (GLib.List<Files.File> files) {
        if (files == null || files.length () == 0)
            return;

        if (image_previewer == null) {
            image_previewer = get_widget_by_name (window, "frame-previewer");
            if (image_previewer != null)
                image_previewer.button_press_event.connect (play_audio_on_previewer_click);
        }
    }

    private void play_audio_on_click () {
        unowned var files =  slot_view.get_selected_files ();
        if (files == null || files.length () > 1)
            return;

        var ftype = files.data.get_ftype ();
        if (ftype.contains ("audio")) {
            Gst.State state;
            pipeline.get_state (out state, null, Gst.CLOCK_TIME_NONE);
            if (state == Gst.State.NULL)
                on_play_pause_cb ();
            else {
                play_audio ();
            }
        }
    }

#if DISABLE_OPEN_AUDIOS
    private void play_audio_on_open_click () {
        play_audio_on_click ();
    }
#endif

    private bool play_audio_on_previewer_click () {
        play_audio_on_click ();
        return true;
    }

    private Gtk.Widget? get_widget_by_name (Gtk.Widget parent, string name) {
        if (parent.get_name () == name)
            return parent;
        else if (parent is Gtk.Container) {
            var children = ((Gtk.Container)parent).get_children ();
            foreach (var c in children) {
                var child = get_widget_by_name (c, name);
                if (child != null)
                    return child;
            }
        } else if (parent is Gtk.Bin) {
            var child = ((Gtk.Bin)parent).get_child ();
            return get_widget_by_name (child, name);
        }
        return null;
    }
}

public Files.Plugins.Base module_init () {
    return new Files.Plugins.Medias ();
}
